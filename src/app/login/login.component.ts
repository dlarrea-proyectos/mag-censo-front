import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { fieldValidator, formValidator } from 'src/utils/formValidator';
import { Response } from '../interfaces/Response';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  rememberMe: boolean = false;
  showResetPass: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  form : FormGroup;
  formResetPass : FormGroup;
  loading: boolean = false;
  loadingReset: boolean = false;
  @ViewChild('captcha') captchaElement!:ElementRef;
  
  constructor(private route: ActivatedRoute, private authService: AuthenticationService, 
    private messageService: MessageService, private formBuilder: FormBuilder, private router: Router) {
    
    this.form = this.formBuilder.group({
      email: ['', [Validators.required]],
      clave: ['', [Validators.required]],
    });

    let email = localStorage.getItem('email');
    if(email) {
      this.rememberMe = true;
      this.form.controls['email'].setValue(email)
    };
    
    this.formResetPass = this.formBuilder.group({
      email: ['', [Validators.required]],
      captcha: ['', [Validators.required]],
    });

    this.route.queryParams.subscribe(params => {
      if(params['key']){
        this.authService.activate(params['key']).subscribe({
          next: (value: Response) => {
            this.messageService.add({key: 'login', severity:'success', summary:'Activación de cuenta', detail: 'Su cuenta ha sido activada con éxito', life: 10000, closable: true});
          },
          error: (err: HttpErrorResponse) => {
            let error: Response = err.error;
            if(Array.isArray(error.message)){
              error.message.forEach(m => {
                this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
              })
            } else {
              this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
            }
            this.loading = false;
          }
        })
      }
    });
  }

  ngOnInit(): void {
  }

  login = () => {
    if(this.form.valid){

      if(this.rememberMe) localStorage.setItem('email', this.form.controls['email'].value);
      else localStorage.removeItem('email');

      this.loading = true;
      this.authService.login(this.form.value).subscribe({
        next: (response: Response) => {
          this.authService.setUser(response.data?.access_token);
          this.loading = false;
          this.form.reset();
          this.router.navigate(['censo/evaluacion']);
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loading = false;
        }
      })
    }
  }

  reset = () => {
    if(this.formResetPass.valid){
      this.loadingReset = true;
      this.authService.cambiarClaveSolicitud(this.formResetPass.value).subscribe({
        next: (response: Response) => {
          this.loadingReset = false;
          this.formResetPass.reset();
          this.messageService.add({key: 'login', severity:'success', summary:'Solicitud recibida', detail: 'Se ha enviado un enlace a su dirección de correo para resetear su contraseña', life: 10000, closable: true});
          this.showResetPass = false;
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'login', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loadingReset = false;
          this.formResetPass.reset();
          this.setCaptcha();
        }
      })
    }
  }

  setCaptcha = () => {
    let url = `${environment.apiUrl}/authentication/captcha?update=${new Date().getTime()}`;
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    img.src = url;
  }

  openResetPass = () => {
    this.setCaptcha();
    this.showResetPass = true;
  }
}
