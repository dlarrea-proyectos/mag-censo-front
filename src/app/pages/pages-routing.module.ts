import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'evaluacion',
        pathMatch: 'full'
      },
      {
        path: 'evaluacion',
        loadChildren: () => import('./preguntas/preguntas.module').then(m => m.PreguntasModule)
      },
      {
        path: 'datos',
        loadChildren: () => import('./cv/cv.module').then(m => m.CvModule)
      },
      {
        path: 'configuraciones',
        loadChildren: () => import('./configuraciones/configuraciones.module').then(m => m.ConfiguracionesModule)
      },
      {
        path: 'mi-perfil',
        loadChildren: () => import('./mi-perfil/mi-perfil.module').then(m => m.MiPerfilModule)
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./usuarios/usuarios.module').then(m => m.UsuariosModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
