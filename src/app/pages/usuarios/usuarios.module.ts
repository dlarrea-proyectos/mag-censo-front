import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { UsuariosAgregarComponent } from './usuarios-agregar/usuarios-agregar.component';
import { UsuariosVerDatosComponent } from './usuarios-ver-datos/usuarios-ver-datos.component';

//Primeng
import { CardModule } from 'primeng/card';
import { AvatarModule } from 'primeng/avatar';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {ChipsModule} from 'primeng/chips';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {FileUploadModule} from 'primeng/fileupload';
import {MenuModule} from 'primeng/menu';
import {SidebarModule} from 'primeng/sidebar';

@NgModule({
  declarations: [
    UsuariosComponent,
    UsuariosAgregarComponent,
    UsuariosVerDatosComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    AvatarModule,
    ButtonModule,
    DialogModule,
    ToastModule,
    InputTextModule,
    TableModule,
    TagModule,
    TooltipModule,
    ButtonModule,
    DropdownModule,
    MultiSelectModule,
    ChipsModule,
    FileUploadModule,
    ConfirmPopupModule,
    MenuModule,
    SidebarModule,
    FormsModule,
    ReactiveFormsModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
