import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Response } from 'src/app/interfaces/Response';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { RolService } from 'src/app/services/rol.service';
import { strongPass } from 'src/app/validators/pass-strong.validator';
import { environment } from 'src/environments/environment';
import { fieldValidator, formValidator } from 'src/utils/formValidator';

@Component({
  selector: 'app-usuarios-agregar',
  templateUrl: './usuarios-agregar.component.html',
  styleUrls: ['./usuarios-agregar.component.scss']
})
export class UsuariosAgregarComponent implements OnInit, AfterViewInit {

  formValidator = formValidator;
  fieldValidator = fieldValidator;
  form : FormGroup;
  loading: boolean = false;
  success: boolean = false;
  @ViewChild('captcha') captchaElement!:ElementRef;
  list: any[] = [];

  constructor(private formBuilder:FormBuilder, private authService: AuthenticationService, 
    private messageService: MessageService, private router: Router, private rolService: RolService, 
    private readonly changeDetectorRef: ChangeDetectorRef) {
    this.form = this.formBuilder.group({
      documento: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      clave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      repetirClave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      captcha: ['', [Validators.required]],
      rol: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getRoles();
  }

  ngAfterViewInit(): void {
    this.setCaptcha();
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  clavesIguales = (): boolean =>  {
    let clave = this.form?.controls['clave'].value;
    let repetirClave = this.form?.controls['repetirClave'].value;
    return clave != '' && repetirClave != '' && clave === repetirClave;
  }

  setCaptcha = () => {
    let url = `${environment.apiUrl}/authentication/captcha?update=${new Date().getTime()}`;
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    img.src = url;
  }

  registrarse = () => {
    if(this.form.valid){
      let rol = {...this.form.value.rol};
      let datos = this.form.value;
      datos.rol = rol.id;
      this.loading = true;
      this.authService.register(datos).subscribe({
        next: (response: Response) => {
          this.success = true;
          this.loading = false;
          this.setCaptcha();
          this.form.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'register-admin', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'register-admin', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loading = false;
          this.setCaptcha();
        }
      });
    }
  }

  onHideSuccess = () => {
    this.form.reset();
    this.setCaptcha();
    this.router.navigate(['/usuarios']);
  }

  getRoles = () => {
    this.rolService.getAll().subscribe({
      next: (response: Response) => {
        this.list = response.data.map((el:any)=>{return {nombre: el.nombre, id: el.id}});
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
      }
    })
  }
  
}
