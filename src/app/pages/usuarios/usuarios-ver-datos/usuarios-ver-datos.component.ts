import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/interfaces/User';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { EstadosService } from 'src/app/services/estados.service';
import { RolService } from 'src/app/services/rol.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
import { fieldValidator, formValidator } from 'src/utils/formValidator';

@Component({
  selector: 'app-usuarios-ver-datos',
  templateUrl: './usuarios-ver-datos.component.html',
  styleUrls: ['./usuarios-ver-datos.component.scss']
})
export class UsuariosVerDatosComponent implements OnInit, AfterViewChecked {

  fileUrl = `${environment.apiUrl}/user/archivos-admin`;
  token = localStorage.getItem('token');
  uploadedFiles: any[] = [];
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  userId: number | null = null;
  form : FormGroup;
  user: User | null = null;
  ciudades: any[] = [];
  loading: boolean = false;
  optionSexo = [
    {
      nombre: 'Masculino',
      valor: 'M'
    },
    {
      nombre: 'Femenino',
      valor: 'F'
    }
  ];
  optionEstadoCivil = [
    {
      nombre: 'SOLTERO/A',
      valor: 'SOLTERO/A'
    },
    {
      nombre: 'CASADO/A',
      valor: 'CASADO/A'
    },
    {
      nombre: 'DIVORCIADO/A',
      valor: 'DIVORCIADO/A'
    },
    {
      nombre: 'VIUDO/A',
      valor: 'VIUDO/A'
    }
  ];
  estados: any[] = [];
  verHistorial: boolean = false;
  logs:any[] = [];
  roles: any[] = [];

  constructor(private userService: UserService, private formBuilder:FormBuilder, private estadoService: EstadosService,
    private route: ActivatedRoute, private router: Router, private messageService: MessageService, 
    private ciudadService: CiudadesService, private readonly changeDetectorRef: ChangeDetectorRef, private rolService: RolService) {
    this.form = this.formBuilder.group({
      documento: [{value: '', disabled: true}, [Validators.required, Validators.pattern('[0-9]*')]],
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      fechaNacimiento: ['', [Validators.required]],
      estadoCivil: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      CiudadId: [null, [Validators.required]],
      EstadoId: [null, [Validators.required]],
      direccion: ['', [Validators.required]],
      indigena: [null],
      RolId: [null, [Validators.required]],
      celular: ['', [Validators.required, Validators.pattern('5959[6-9]\\d[\\s\\D]*\\d{3}[\\s\\D]*\\d{3}')]],
    });
    
    if(!this.route.snapshot.paramMap.get('id') || isNaN(Number(this.route.snapshot.paramMap.get('id')))){
      this.router.navigate(['/censo/usuarios'])
    } else {
      this.userId = Number(this.route.snapshot.paramMap.get('id'));
      this.getDatos(this.userId);
      this.getArchivos();
    }
  }
  
  ngOnInit(): void {
  }
  
  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }
  
  getDatos = (id:number) => {
    let estados$ = this.estadoService.get();
    let ciudades$ = this.ciudadService.get();
    let userData$ = this.userService.datosUsuarioAdmin(id);
    let roles$ = this.rolService.getAll();
    forkJoin([ciudades$, estados$, roles$, userData$]).subscribe({
      next: (results: Response[]) => {
        this.ciudades = results[0].data;
        this.estados = results[1].data;
        this.roles = results[2].data.map((el:any) => {return {nombre: el.nombre, id: el.id}});
        this.user = results[3].data;
        this.logs = this.user?.logs.map((el:any) => {return {...el, c: el.comentario.split(';')}}) || [];
        setTimeout(()=> {
          this.patchValues();
        }, 0)
      },
      error: (errors: HttpErrorResponse[]) => {
        console.log(errors);
      }
    })
  }

  patchValues = () => {

    this.form.controls['documento'].patchValue(this.user?.persona?.documento);
    this.form.controls['nombres'].patchValue(this.user?.persona?.nombres);
    this.form.controls['apellidos'].patchValue(this.user?.persona?.apellidos);
    this.form.controls['sexo'].patchValue(this.user?.persona?.sexo );
    this.form.controls['estadoCivil'].patchValue(this.user?.persona?.estadoCivil );
    this.form.controls['fechaNacimiento'].patchValue(this.user?.persona?.fechaNacimiento );
    this.form.controls['celular'].patchValue(this.user?.persona?.celular);
    this.form.controls['direccion'].patchValue(this.user?.persona?.direccion);
    this.form.controls['indigena'].patchValue(this.user?.persona?.indigena);
    this.user?.persona?.CiudadId && 
    this.form.controls['CiudadId'].patchValue(this.ciudades.find(el => el.id === this.user?.persona?.CiudadId));
    this.user?.EstadoId && 
    this.form.controls['EstadoId'].patchValue(this.estados.find(el => el.id === this.user?.EstadoId));
    this.user?.rol?.nombre && 
    this.form.controls['RolId'].patchValue(this.roles.find(el => el.nombre === this.user?.rol?.nombre));
    
    console.log(this.form.value);
  }

  guardar = () => {
    if(this.form.valid){
      this.loading = true;
      let ciudad = {...this.form.value.CiudadId};
      let estado = {...this.form.value.EstadoId};
      let rol = {...this.form.value.RolId};
      let datos = {...this.form.value, id: this.userId};
      datos.CiudadId = ciudad.id;
      datos.EstadoId = estado.id;
      datos.RolId = rol.id;
      this.userService.guardarDatosUsuarioAdmin(datos).subscribe({
        next: (response: Response) => {
          this.loading = false;
          this.messageService.add({key: 'usuarios-ver-datos', severity:'success', summary:'Éxito', detail: 'Datos actualizados'});
          this.getDatos(<number>this.userId);
        }, 
        error: (err:HttpErrorResponse) => {
          this.loading = false;
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'usuarios-ver-datos', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'usuarios-ver-datos', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      });
    }
  }

  getArchivos = () => {
    this.loading = true;
    this.userService.archivosUsuario().subscribe({
      next: (response: Response) => {
        this.loading = false;
        this.uploadedFiles = response.data;
      }, 
      error: (err:HttpErrorResponse) => {
        this.loading = false;
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'usuarios-ver-datos', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'usuarios-ver-datos', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    });
  }

}
