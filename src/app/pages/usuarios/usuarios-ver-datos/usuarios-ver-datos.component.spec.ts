import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosVerDatosComponent } from './usuarios-ver-datos.component';

describe('UsuariosVerDatosComponent', () => {
  let component: UsuariosVerDatosComponent;
  let fixture: ComponentFixture<UsuariosVerDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsuariosVerDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosVerDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
