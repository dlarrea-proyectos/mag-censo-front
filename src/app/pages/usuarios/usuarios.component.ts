import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FileUpload } from 'primeng/fileupload';
import { Table } from 'primeng/table';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/models/User.model';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  @ViewChild('importar') importar!: FileUpload;
  fileUrl = `${environment.apiUrl}/user/exportar`;
  fileUrlImportar = `${environment.apiUrl}/user/importar`;
  fileUrlTemplate = `${environment.apiUrl}/user/template`;
  token = localStorage.getItem('token');
  list: User[] = [];
  totalList: number = 0;
  search: string = '';
  showImportarResult: boolean = false;
  errorImportarResult: boolean = false;
  logs: any[] = [];

  constructor(private userService: UserService, private loader: NgxUiLoaderService) {}

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.userService.getAll().subscribe({
      next: (response: Response) => {
        this.list = response.data;
        this.totalList = response.meta?.total || 0;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  exportFile = () => {
    window.open(`${this.fileUrl}?key=${this.token}`, '_blank');
  }

  clear(table: Table) {
    table.clear();
    this.search = '';
  }

  importarClick = () => {
    this.importar.choose();
  }

  onUpload = (event:any) => {
    console.log(event);
    this.loader.stop();
    let originalEvent: HttpResponse<any> = event.originalEvent;
    if(originalEvent.body){
      this.logs = originalEvent.body?.data;
    } else {
      this.errorImportarResult = true;
    }
    this.showImportarResult = true;
    this.get();
  }
  
  onError = (event:any) => {
    console.log(event);
  }

  onProgress = (event:any) => {
    this.loader.start();
  }
}
