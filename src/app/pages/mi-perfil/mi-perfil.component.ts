import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/interfaces/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { strongPass } from 'src/app/validators/pass-strong.validator';
import { environment } from 'src/environments/environment';
import { fieldValidator, formValidator } from 'src/utils/formValidator';

@Component({
  selector: 'app-mi-perfil',
  templateUrl: './mi-perfil.component.html',
  styleUrls: ['./mi-perfil.component.scss']
})
export class MiPerfilComponent implements OnInit {

  user: User | null = null;
  nombre: string = '';
  @ViewChild('captcha') captchaElement!:ElementRef;
  form : FormGroup;
  loadingReset: boolean = false;
  showResetPass: boolean = false;
  foto: any = null;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  fileUrl = `${environment.apiUrl}/user/archivos`;
  token = localStorage.getItem('token');
  codigo: string = '';

  constructor(private authService: AuthenticationService, private messageService: MessageService, 
    private formBuilder: FormBuilder, private userService: UserService) {
    this.form = this.formBuilder.group({
      clave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      repetirClave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      captcha: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getFoto();
    this.authService.user.subscribe(
      data => {
        this.user = data;
        if(this.user){
          try {
            this.nombre = `${this.user.nombres.split(' ')[0]} ${this.user.apellidos.split(' ')[0]}`;
            this.codigo = btoa(String(this.user?.email));
          } catch(e){
            this.nombre = this.user.email;
          }
        } else {
          this.authService.getUser();
        }
      }
    )
  }

  cerrarSesion = () => {
    this.authService.logout();
  }

  reset = () => {
    if(this.form.valid){
      this.loadingReset = true;
      this.userService.cambiarClave(this.form.value).subscribe({
        next: (response: Response) => {
          this.loadingReset = false;
          this.form.reset();
          this.messageService.add({key: 'perfil', severity:'success', summary:'Cambio de contraseña', detail: 'Su contraseña ha sido cambiada con éxito', life: 10000, closable: true});
          this.showResetPass = false;
          this.cerrarSesion();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'perfil', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'perfil', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loadingReset = false;
          this.form.reset();
          this.setCaptcha();
        }
      })
    }
  }

  setCaptcha = () => {
    let url = `${environment.apiUrl}/authentication/captcha?update=${new Date().getTime()}`;
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    img.src = url;
  }

  openResetPass = () => {
    this.setCaptcha();
    this.showResetPass = true;
  }

  clavesIguales = (): boolean =>  {
    let clave = this.form?.controls['clave'].value;
    let repetirClave = this.form?.controls['repetirClave'].value;
    return clave != '' && repetirClave != '' && clave === repetirClave;
  }

  getFoto = () => {
    this.userService.getFoto().subscribe({
      next: (response: Response) => {
        if(response.data){
          this.foto = response.data;
        }
      },
      error: (err:HttpErrorResponse) => {
        this.foto = null;
      }
    })
  }
}
