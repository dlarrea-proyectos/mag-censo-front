import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MiPerfilRoutingModule } from './mi-perfil-routing.module';
import { MiPerfilComponent } from './mi-perfil.component';


//Primeng
import { CardModule } from 'primeng/card';
import { AvatarModule } from 'primeng/avatar';
import { ButtonModule } from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';
import {InputTextModule} from 'primeng/inputtext';
import {FileUploadModule} from 'primeng/fileupload';

@NgModule({
  declarations: [
    MiPerfilComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    AvatarModule,
    ButtonModule,
    DialogModule,
    ToastModule,
    InputTextModule,
    FileUploadModule,
    ReactiveFormsModule,
    MiPerfilRoutingModule
  ]
})
export class MiPerfilModule { }
