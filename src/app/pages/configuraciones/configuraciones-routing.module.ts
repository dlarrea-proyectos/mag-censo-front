import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { ConfiguracionesComponent } from './configuraciones.component';
import { EstadosComponent } from './estados/estados.component';

const routes: Routes = [
  {
    path: '',
    component: ConfiguracionesComponent,
    children: [
      {
        path: '',
        redirectTo: 'estados',
        pathMatch: 'full'
      },
      {
        path: 'estados',
        component: EstadosComponent
      },
      {
        path: 'ciudades',
        component: CiudadesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionesRoutingModule { }
