import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Response } from 'src/app/interfaces/Response';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { fieldValidator, formValidator } from 'src/utils/formValidator';

@Component({
  selector: 'app-ciudades',
  templateUrl: './ciudades.component.html',
  styleUrls: ['./ciudades.component.scss']
})
export class CiudadesComponent implements OnInit {

  list: any[] = [];
  formAdd : FormGroup;
  formEdit : FormGroup;
  elementDelete: any = null;
  elementAddShow: boolean = false;
  elementEditShow: boolean = false;
  elementAddLoading: boolean = false;
  elementEditLoading: boolean = false;
  departamentos: any[] = ['CAPITAL', 'CONCEPCIÓN', 'SAN PEDRO', 'CORDILLERA', 'GUAIRÁ', 'CAAGUAZÚ', 'CAAZAPÁ', 'ITAPÚA', 'MISIONES', 'PARAGUARÍ', 'ALTO PARANÁ', 'CENTRAL', 'ÑEEMBUCÚ', 'AMAMBAY', 'CANINDEYÚ', 'PRESIDENTE HAYES', 'BOQUERÓN', 'ALTO PARAGUAY'];
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  search: string = '';

  constructor(private ciudadService: CiudadesService, private formBuilder: FormBuilder, private messageService: MessageService, 
    private confirmationService: ConfirmationService, private loader: NgxUiLoaderService) {
    this.formAdd = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
    });
    this.formEdit = this.formBuilder.group({
      id: [null, [Validators.required]],
      nombre: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.ciudadService.get().subscribe({
      next: (response: Response) => {
        this.list = response.data;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }
  
  add = () => {
    if(this.formAdd.valid){
      this.elementAddLoading = true;
      this.ciudadService.post(this.formAdd.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'conf', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.elementAddShow = false;
          this.elementAddLoading = false;
          this.formAdd.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  edit = () => {
    if(this.formEdit.valid){
      this.elementEditLoading = true;
      this.ciudadService.put(this.formEdit.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'conf', severity:'success', summary:'Éxito', detail: 'Elemento editado'});
          this.elementEditShow = false;
          this.elementEditLoading = false;
          this.formEdit.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          this.elementEditLoading = false;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  delete = (id:number) => {
    this.ciudadService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'conf', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 
  

  setEdit(element:any){
    this.formEdit.controls['nombre'].setValue(element.nombre);
    this.formEdit.controls['departamento'].setValue(element.departamento);
    this.formEdit.controls['id'].setValue(element.id);
    this.elementEditShow = true;
  }

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'conf-pop',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }

  clear(table: Table) {
    table.clear();
    this.search = '';
  }

}
