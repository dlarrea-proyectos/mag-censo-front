import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-configuraciones',
  templateUrl: './configuraciones.component.html',
  styleUrls: ['./configuraciones.component.scss']
})
export class ConfiguracionesComponent implements OnInit {

  items: MenuItem[] = [
    {
      label: 'Estados',
      icon: 'pi pi-flag-fill',
      routerLink: 'estados'
    },
    {
      label: 'Ciudades',
      icon: 'pi pi-building',
      routerLink: 'ciudades'
    }
  ]  
  constructor() { }

  ngOnInit(): void {
  }

}
