import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionesRoutingModule } from './configuraciones-routing.module';
import { ConfiguracionesComponent } from './configuraciones.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EstadosComponent } from './estados/estados.component';

//Primeng
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import {TabMenuModule} from 'primeng/tabmenu';
import { CardModule } from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [
    ConfiguracionesComponent,
    EstadosComponent,
    CiudadesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    TabMenuModule,
    CardModule,
    DialogModule,
    ToastModule,
    DropdownModule,
    ConfirmPopupModule,
    ReactiveFormsModule,
    ConfiguracionesRoutingModule
  ]
})
export class ConfiguracionesModule { }
