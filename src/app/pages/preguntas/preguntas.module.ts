import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreguntasRoutingModule } from './preguntas-routing.module';
import { PreguntasComponent } from './preguntas.component';
import { ReactiveFormsModule } from '@angular/forms';

//Primeng
import { CardModule } from 'primeng/card';
import {MessagesModule} from 'primeng/messages';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DividerModule} from 'primeng/divider';
import {DropdownModule} from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    PreguntasComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    RadioButtonModule,
    MessagesModule,
    DividerModule,
    DropdownModule,
    ButtonModule,
    InputTextModule,
    ToastModule,
    ReactiveFormsModule,
    PreguntasRoutingModule
  ]
})
export class PreguntasModule { }
