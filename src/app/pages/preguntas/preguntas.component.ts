import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/utils/formValidator';
import { Message, MessageService } from 'primeng/api';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Response } from 'src/app/interfaces/Response';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-preguntas',
  templateUrl: './preguntas.component.html',
  styleUrls: ['./preguntas.component.scss']
})
export class PreguntasComponent implements OnInit, AfterViewChecked  {

  info: Message[] = [{
    severity: 'info',
    summary: 'Censo MAG',
    detail: 'Respondé las siguientes preguntas del formulario'
  }];
  form : FormGroup;
  experiencias : any[] = [
    {
      label: '0 a 1 años'
    },
    {
      label: '2 a 3 años'
    },
    {
      label: '4 a 5 años'
    },
    {
      label: '6 a 7 años'
    },
    {
      label: '8 a 9 años'
    },
    {
      label: '10 a 11 años'
    },
    {
      label: '12 a 13 años'
    },
    {
      label: '14 a 15 años'
    },
    {
      label: 'Más de 15 años'
    }
  ];
  medios: any[] = [
    {
      label: 'Redes sociales'
    },
    {
      label: 'Medios radiales'
    },
    {
      label: 'Medios televisivos'
    },
    {
      label: 'Familiares o amigos'
    },
  ];
  ciudades: any[] = [];
  loading: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;

  constructor(private formBuilder: FormBuilder, private ciudadService: CiudadesService, 
    private readonly changeDetectorRef: ChangeDetectorRef, private usuarioService: UserService, 
    private router: Router, private loader: NgxUiLoaderService, private messageService: MessageService) {
    this.form = this.formBuilder.group({
      p1: [null, [Validators.required]],
      p2: [null, [Validators.required]],
      p3: [null, [Validators.required]],
      p4: [null, [Validators.required]],
      p5: [ null, [Validators.required]],
      p6: [ null, [Validators.required]],
      p7: [ null, [Validators.required]],
      p8: [ null, [Validators.required]],
      p9: [ null, [Validators.required]],
      p10: [ null, [Validators.required]],
      p11: [ null, [Validators.required]],
      p12: [ null, [Validators.required]],
      p13: [ null, [Validators.required]],
      p14: [ null, [Validators.required]],
      p15: [ null, [Validators.required]],
      p16: [ null, [Validators.required]],
      p17: [ null, [Validators.required]],
      p18: [ null],
    });
  }

  async ngOnInit() {
    this.getCiudades();
    this.loader.start();
    await this.getUserEvaluacion();
    this.loader.stop();
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  evaluar = () => {
    if(this.form.valid){
      this.loading = true;
      let ciudad = {...this.form.value.p12};
      let datos = this.form.value;
      datos.p12 = `${ciudad.nombre} - ${ciudad.departamento}`;
      datos.p18 = ciudad.id;
      this.usuarioService.evaluacion(datos).subscribe({
        next: (value: Response) => {
          this.router.navigate(['censo', 'datos']);
          this.loading = true;
        },
        error: (err: HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'preguntas', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'preguntas', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loading = false;
        }
      })
    } else {
      this.checkRequired();
    }
  }

  checkRequired = () => {
    Object.keys(this.form.controls).forEach(key => {
      if(this.form.controls[key].invalid) this.form.controls[key].markAsDirty();
    });
  }

  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (value: Response) => {
        this.ciudades = value.data;
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });  
  }

  getUserEvaluacion = async() => {
    return await new Promise((resolve:any) => {
      this.usuarioService.getEvaluacion().subscribe({
        next: (value: Response) => {
          if(value.data !== 0) this.router.navigate(['censo', 'datos']);
          resolve();
        },
        error: (err: HttpErrorResponse) => {
          resolve();
        }
      });
    });
  }
}
