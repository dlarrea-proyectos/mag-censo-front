import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPermissionsModule } from 'ngx-permissions';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { NavbarComponent } from './navbar/navbar.component';


//Primeng
import {AvatarModule} from 'primeng/avatar';
import {TooltipModule} from 'primeng/tooltip';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {ButtonModule} from 'primeng/button';
import {SidebarModule} from 'primeng/sidebar';
import {ScrollTopModule} from 'primeng/scrolltop';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    PagesComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    AvatarModule,
    TooltipModule,
    TieredMenuModule,
    ButtonModule,
    ScrollTopModule,
    SidebarModule,
    NgxPermissionsModule.forChild(),
    PagesRoutingModule
  ]
})
export class PagesModule { }
