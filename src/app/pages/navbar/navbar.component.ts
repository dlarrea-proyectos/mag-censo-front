import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { MenuItem } from 'primeng/api';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/interfaces/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  sidebarItems: any[] = [
    {
      nombre: 'Inicio',
      icon: 'pi pi-home',
      routerLink: '/censo',
      perm: ['ADMIN', 'VISITANTE'],
      command: () => {
        this.displaySidebar = false;
      }
    },
    {
      nombre: 'Mi perfil',
      icon: 'pi pi-user',
      routerLink: '/censo/mi-perfil',
      perm: ['ADMIN', 'VISITANTE'],
      command: () => {
        this.displaySidebar = false;
      }
    },
    {
      nombre: 'Usuarios',
      icon: 'pi pi-users',
      routerLink: '/censo/usuarios',
      perm: ['ADMIN'],
      command: () => {
        this.displaySidebar = false;
      }
    },
    {
      nombre: 'Configuraciones',
      icon: 'pi pi-cog',
      routerLink: '/censo/configuraciones',
      perm: ['ADMIN'],
      command: () => {
        this.displaySidebar = false;
      }
    },
    {
      nombre: 'Cerrar Sesión',
      icon: 'pi pi-sign-out',
      routerLink: null,
      perm: ['ADMIN', 'VISITANTE'],
      command: () => {
        this.authService.logout();
      }
    },
  ];
  items: MenuItem[] = [
    {
      label: 'Mi perfil',
      icon: 'pi pw pi-user',
      command: () => {
        this.router.navigate(['censo', 'mi-perfil']);
      }
    },
    {
      label: 'Cerrar Sesión',
      icon: 'pi pw pi-sign-out',
      command: () => {
        this.authService.logout();
      }
    },
  ];
  user: User | null = null;
  nombre: string = '';
  displaySidebar: boolean = false;
  foto: any = null;
  fotoIsSet: boolean = false;
  fileUrl = `${environment.apiUrl}/user/archivos`;
  token = localStorage.getItem('token');

  constructor(private authService: AuthenticationService, private permissionService: NgxPermissionsService, 
    private router: Router, private userService: UserService) {
    this.authService.user.subscribe(
      data => {
        this.user = data;
        if(this.user != null){
          try {
            this.permissionService.loadPermissions([this.user.rol]);
            this.nombre = `${this.user.nombres.split(' ')[0]} ${this.user.apellidos.split(' ')[0]}`;
            if(!this.fotoIsSet){
              this.fotoIsSet = true;
              this.getFoto();
            }
          } catch(e){
            this.nombre = this.user.email;
          }
        }
      }
    )
  }

  ngOnInit(): void {
  }

  getFoto = () => {
    this.userService.getFoto().subscribe({
      next: (response: Response) => {
        if(response.data){
          this.foto = response.data;
        }
      },
      error: (err:HttpErrorResponse) => {
        this.foto = null;
      }
    })
  }
}
