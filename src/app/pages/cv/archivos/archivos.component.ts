import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/interfaces/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.scss']
})
export class ArchivosComponent implements OnInit {

  fileUrl = `${environment.apiUrl}/user/archivos`;
  token = localStorage.getItem('token');
  uploadedFiles: any[] = [];
  loading: boolean = false;
  headers: HttpHeaders = new HttpHeaders();
  tipo: string | null = null;
  codigo: string = '';
  user: User | null = null;
  userIsSet: boolean = false;
  tipos = [
    'Foto de perfil',
    'Foto CI Frontal',
    'Foto CI Dorsal',
    'Certificado de antecedentes policiales',
    'Certificado de vida y residencia',
    'Copia de título universitario',
    'Constancia/Certificado de trabajo/experiencia',
    'Certificado cumplimiento tributario',
    'Foto de factura',
    'Copia de certificados de cursos de capacitación',
  ]

  constructor(private authService: AuthenticationService, private userService: UserService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.authService.user.subscribe(
      data => {
        if(data != null && !this.userIsSet){
          this.user = data;
          this.codigo = btoa(String(this.user?.email));
          this.userIsSet = true; 
        }
    })
  }

  ngOnInit(): void {
    this.getArchivos();
  }

  onError(event:any) {
    let err: Response = event.error?.error;
    if(err){
      this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: err.message});
    }
  }

  onUpload(event:any){
    this.headers = new HttpHeaders();
    this.tipo = null;
    this.getArchivos();
  }

  getArchivos = () => {
    this.loading = true;
    this.userService.archivosUsuario().subscribe({
      next: (response: Response) => {
        this.loading = false;
        this.uploadedFiles = response.data;
      }, 
      error: (err:HttpErrorResponse) => {
        this.loading = false;
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    });
  }

  confirm(event: Event, f:any) {
    this.confirmationService.confirm({
      key: 'file',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar archivo?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.eliminarArchivo(f.file);
      }
    });
  }

  eliminarArchivo = (file:string) => {
    this.userService.eliminarArchivoUsuario(file).subscribe({
      next: (response: Response) => {
        this.getArchivos();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  onChangeTipo = (event:any) => {
    this.headers = this.headers.set('tipo', <string>this.tipo);
  }

  copiar = () => {
    navigator.clipboard.writeText(this.codigo).then(() => {
      this.messageService.add({key: 'cv', severity:'success', summary:'Cód. postulación', detail: 'Copiado'});
      console.log('Codigo de postulacion copiado');
    }, (err) => {
      console.error('Codigo de postulacion no copiado ', err);
    });
  }

  mailTo = () => {
    let link = document.createElement('a');
    link.href = 'mailto:mag.censo@gmail.com';
    link.click();
  }
}
