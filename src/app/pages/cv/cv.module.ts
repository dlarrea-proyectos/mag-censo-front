import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CvRoutingModule } from './cv-routing.module';
import { CvComponent } from './cv.component';
import { DatosComponent } from './datos/datos.component';
import { ArchivosComponent } from './archivos/archivos.component';

//Primeng
import {StepsModule} from 'primeng/steps';
import {ToastModule} from 'primeng/toast';
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';
import {AvatarModule} from 'primeng/avatar';
import {FileUploadModule} from 'primeng/fileupload';
import {MessagesModule} from 'primeng/messages';
import {TooltipModule} from 'primeng/tooltip';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {DividerModule} from 'primeng/divider';
import { TagModule } from 'primeng/tag';

@NgModule({
  declarations: [
    CvComponent,
    DatosComponent,
    ArchivosComponent
  ],
  imports: [
    CommonModule,
    ToastModule,
    StepsModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    AvatarModule,
    ButtonModule,
    FileUploadModule,
    MessagesModule,
    FormsModule,
    TooltipModule,
    TagModule,
    DividerModule,
    ConfirmPopupModule,
    AutoCompleteModule,
    ReactiveFormsModule,
    CvRoutingModule
  ]
})
export class CvModule { }
