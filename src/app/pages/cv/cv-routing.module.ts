import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArchivosComponent } from './archivos/archivos.component';
import { CvComponent } from './cv.component';
import { DatosComponent } from './datos/datos.component';

const routes: Routes = [
  {
    path: '',
    component: CvComponent,
    children: [
      {
        path: '',
        component: DatosComponent
      },
      {
        path: 'archivos',
        component: ArchivosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CvRoutingModule { }
