import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { Response } from 'src/app/interfaces/Response';
import { User } from 'src/app/interfaces/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { UserService } from 'src/app/services/user.service';
import { fieldValidator, formValidator } from 'src/utils/formValidator';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.scss']
})
export class DatosComponent implements OnInit {

  formValidator = formValidator;
  fieldValidator = fieldValidator;
  form : FormGroup;
  user: User | null = null;
  userIsSet: boolean = false;
  optionSexo = [
    {
      nombre: 'Masculino',
      valor: 'M'
    },
    {
      nombre: 'Femenino',
      valor: 'F'
    }
  ];
  optionEstadoCivil = [
    {
      nombre: 'SOLTERO/A',
      valor: 'SOLTERO/A'
    },
    {
      nombre: 'CASADO/A',
      valor: 'CASADO/A'
    },
    {
      nombre: 'DIVORCIADO/A',
      valor: 'DIVORCIADO/A'
    },
    {
      nombre: 'VIUDO/A',
      valor: 'VIUDO/A'
    }
  ];
  loading: boolean = false;
  ciudades: any[] = [];
  codigo: string = '';

  constructor(private authService: AuthenticationService, private userService: UserService, 
    private messageService: MessageService, private formBuilder:FormBuilder, 
    private ciudadService: CiudadesService, private readonly changeDetectorRef: ChangeDetectorRef) {
    this.authService.user.subscribe(
      data => {
        if(data != null && !this.userIsSet){
          this.user = data;
          this.codigo = btoa(String(this.user?.email));
          this.userIsSet = true;
          this.getDatos(this.user.id)  
        }
    })

    this.form = this.formBuilder.group({
      documento: [{value: '', disabled: true}, [Validators.required, Validators.pattern('[0-9]*')]],
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      fechaNacimiento: ['', [Validators.required]],
      estadoCivil: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      CiudadId: [{value: '', disabled: true}],
      direccion: ['', [Validators.required]],
      indigena: [null],
      celular: ['', [Validators.required, Validators.pattern('5959[6-9]\\d[\\s\\D]*\\d{3}[\\s\\D]*\\d{3}')]],
    });
  }

  ngOnInit(): void {}

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  getDatos = (id:number) => {
    let ciudades$ = this.ciudadService.get();
    let userData$ = this.userService.datosUsuario(id);
    forkJoin([ciudades$, userData$]).subscribe({
      next: (results: Response[]) => {
        this.ciudades = results[0].data;
        this.user = results[1].data;
        setTimeout(()=> {
          this.patchValues();
        }, 0)
      },
      error: (errors: HttpErrorResponse[]) => {
        console.log(errors[0].error);
        console.log(errors[1].error);
      }
    })
  }

  guardar = () => {
    if(this.form.valid){
      this.loading = true;
      let datos = this.form.value;
      this.userService.guardarDatosUsuario(datos).subscribe({
        next: (response: Response) => {
          this.loading = false;
          this.messageService.add({key: 'cv', severity:'success', summary:'Éxito', detail: 'Tus datos fueron actualizados'});
        }, 
        error: (err:HttpErrorResponse) => {
          this.loading = false;
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'cv', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      });
    }
  }

  patchValues = () => {

    this.form.controls['documento'].patchValue(this.user?.persona?.documento);
    this.form.controls['nombres'].patchValue(this.user?.persona?.nombres);
    this.form.controls['apellidos'].patchValue(this.user?.persona?.apellidos);
    this.form.controls['sexo'].patchValue(this.user?.persona?.sexo );
    this.form.controls['estadoCivil'].patchValue(this.user?.persona?.estadoCivil );
    this.form.controls['fechaNacimiento'].patchValue(this.user?.persona?.fechaNacimiento );
    this.form.controls['celular'].patchValue(this.user?.persona?.celular);
    this.form.controls['direccion'].patchValue(this.user?.persona?.direccion);
    this.form.controls['indigena'].patchValue(this.user?.persona?.indigena);
    if(this.user?.persona?.CiudadId)
      for(let el of this.ciudades){
        if(el.id === this.user?.persona?.CiudadId){
          this.form.controls['CiudadId'].patchValue(`${el.nombre} - ${el.departamento}`);
          break;
        }
      }
    
  }

  copiar = () => {
    navigator.clipboard.writeText(this.codigo).then(() => {
      this.messageService.add({key: 'cv', severity:'success', summary:'Cód. postulación', detail: 'Copiado'});
      console.log('Codigo de postulacion copiado');
    }, (err) => {
      console.error('Codigo de postulacion no copiado ', err);
    });
  }

  mailTo = () => {
    let link = document.createElement('a');
    link.href = 'mailto:mag.censo@gmail.com';
    link.click();
  }

}
