import { Component, OnInit } from '@angular/core';
import { MenuItem, Message } from 'primeng/api';
import { User } from 'src/app/interfaces/User';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {

  user: User | null = null;
  items: MenuItem[] = [
    {
      label: 'Datos',
      icon: 'pi pw pi-user',
      routerLink: '/censo/datos'
    },
    {
      label: 'Subir archivos',
      icon: 'pi pw pi-user',
      routerLink: 'archivos'
    }
  ];
  msg: Message[];

  constructor(){
    this.msg = [{severity:'info', summary:'Importante', detail: 'Complete sus datos personales y luego suba sus archivos: CV, Cédula (ambos lados), etc.'}]
  }
  ngOnInit(): void {
  }

}
