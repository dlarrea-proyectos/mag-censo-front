import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';

//Primeng
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [
    LandingComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    LandingRoutingModule
  ]
})
export class LandingModule { }
