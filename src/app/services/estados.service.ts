import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstadosService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/admin/estado`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/admin/estado`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.apiUrl}/admin/estado/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.apiUrl}/admin/estado/${id}`);
  }
  
}
