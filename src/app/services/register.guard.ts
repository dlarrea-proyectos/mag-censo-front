import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterGuard implements CanActivate {
  
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    if(route.queryParams && route.queryParams['cargo']){
      let cargos: string[] = ['Jefe Departamental', 'Jefe Distrital', 'Supervisor', 'Técnico Censista para Grandes Fincas', 'Técnico Capacitador', 'Censista']; 
      if(!cargos.includes(route.queryParams['cargo'])){
        this.router.navigate(['cargos']);
        return false;
      }
      return true;
    }
    this.router.navigate(['cargos']);
    return false;
    
  }
  
}
