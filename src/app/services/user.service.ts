import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  datosUsuario = (id:number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user/${id}/datos`);
  }
  
  datosUsuarioAdmin = (id:number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user/${id}/datos-admin`);
  }
  
  guardarDatosUsuario = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/user/datos`, form);
  }
  
  guardarDatosUsuarioAdmin = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/user/datos-admin`, form);
  }
  
  cambiarClave = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/user/cambiar-clave`, form);
  }
  
  archivosUsuario = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user/archivos`);
  }
  
  eliminarArchivoUsuario = (file:string):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.apiUrl}/user/archivos/${file}`);
  }

  evaluacion = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/user/evaluacion`, form);
  }
  
  getEvaluacion = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user/evaluacion/count`);
  }
  
  getAll = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user`);
  }
  
  getFoto = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/user/foto`);
  }
  
}
