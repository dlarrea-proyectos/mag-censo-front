import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/interfaces/Response';

@Injectable({
  providedIn: 'root'
})
export class CiudadesService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/admin/ciudad`);
  }
  
  getDropDownValues = ():Observable<any> => {
    return this.http.get<Response | any>(`${environment.apiUrl}/admin/ciudad`).pipe(
      map((res: Response) => {
        return res.data.map((el:any) => {return el.nombre});
      }),
      catchError(() => of([]))
    );
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.apiUrl}/admin/ciudad`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.apiUrl}/admin/ciudad/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.apiUrl}/admin/ciudad/${id}`);
  }
}
