import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../interfaces/Response';
import { User } from '../interfaces/User';
import jwt_decode from "jwt-decode";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private router: Router) { }

  private userSource = new BehaviorSubject<User | null>(null);
  public user = this.userSource.asObservable();

  register = (form:any):Observable<Response> => {
    return this.http.post<Response>(`${environment.apiUrl}/authentication/register`, form);
  }

  activate = (key:string):Observable<Response> => {
    return this.http.post<Response>(`${environment.apiUrl}/authentication/activate`, {key});
  }

  login = (form:any):Observable<Response> => {
    return this.http.post<Response>(`${environment.apiUrl}/authentication/login`, form);
  }
  
  cambiarClaveSolicitud = (form:any):Observable<Response> => {
    return this.http.post<Response>(`${environment.apiUrl}/authentication/cambiar-clave-solicitud`, form);
  }
  
  cambiarClave = (form:any):Observable<Response> => {
    return this.http.post<Response>(`${environment.apiUrl}/authentication/cambiar-clave`, form);
  }

  setUser = (access_token: string) => {
    let data: User = jwt_decode(access_token);
    localStorage.setItem('token', access_token);
    this.userSource.next(data);
  }
  
  getUser = () => {
    let token = localStorage.getItem('token');
    if(token != null){
      let data: User = jwt_decode(token);
      this.userSource.next(data);
    }
  }

  logout = () => {
    localStorage.clear();
    this.userSource.next(null);
    this.router.navigate(['/login']);
  }

  tokenIsExpired = () => {
    let token = localStorage.getItem('token');
    if(token == null) return false;
    let data: User = jwt_decode(token);
    if (Date.now() >= data.exp * 1000) {
      return false;
    }
    return true;
  }
}
