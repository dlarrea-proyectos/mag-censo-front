import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-cargos',
  templateUrl: './landing-cargos.component.html',
  styleUrls: ['./landing-cargos.component.scss']
})
export class LandingCargosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  cargos = [
    {
      nombre: 'Jefe Departamental',
      duracion: '4 meses',
      descripcion: `
      <ul>
          <li>
              <small>Egresados de las carreras:</small>
              <ul>
                  <li>
                      <small><b>Ing. Agronómica</b></small>
                  </li>
                  <li>
                      <small><b>Economía y estadística</b></small>
                  </li>
                  <li>
                      <small><b>Ciencias Veterinarias</b></small>                     
                  </li>
                  <li>
                      <small><b>Zootecnia</b></small>                              
                  </li>
                  <li>
                      <small><b>Administración Agraria o carreras afines</b></small>
                  </li>
              </ul>
          </li>
          <li>
            <small>Con experiencia superior a 5 años liderando equipos en trabajos agropecuarios, proyectos o desarrollo rural, censos y/o encuestas</small>
          </li>
          <li>
            <small>Que residan en la zona mencionada y/o alrededores</small>
          </li>
          <li>
            <small>Buen manejo de herramientas informáticas</small>
          </li>	
      </ul>
      `
    },
    {
      nombre: 'Jefe Distrital',
      duracion: '3 meses',
      descripcion: `
      <ul>
          <li>
              <small>Profesional universitario egresado en:</small>
              <ul>
                  <li><small><b>Ing. Agronómica</b></small></li>
                  <li><small><b>Ciencias Veterinarias</b></small></li>
                  <li><small><b>Zootecnia</b></small></li>
                  <li><small><b>Economía</b></small></li>
                  <li><small><b>Administración agraria</b></small></li>
                  <li><small><b>Estadística</b></small></li>
                  <li><small><b>Técnicos de mando medio</b></small></li>
                  <li><small><b>Docente u otras profesiones</b></small></li>
              </ul>
          </li>
          <li><small>Con experiencia de 5 años liderando equipos en trabajo de censos o encuestas, trabajo agropecuario, proyecto o programa de desarrollo rural</small></li>
          <li><small>Que residan en la zona mencionada y/o alrededores</small></li>
          <li><small>Buen manejo de herramientas Informáticas</small></li>
      </ul>
      `
    },
    {
      nombre: 'Supervisor',
      duracion: '2 meses',
      descripcion: `
      <ul>
          <li>
              <small>Profesional universitario egresado en:</small>
              <ul>
                  <li><small><b>Ing. Agronómica</b></small></li>
                  <li><small><b>Ciencias Veterinarias</b></small></li>
                  <li><small><b>Zootecnia</b></small></li>
                  <li><small><b>Economía</b></small></li>
                  <li><small><b>Administración agraria</b></small></li>
                  <li><small><b>Estadística</b></small></li>
                  <li><small><b>Técnicos de mando medio</b></small></li>
                  <li><small><b>Docente u otras profesiones</b></small></li>
              </ul>
          </li>
          <li><small>Con experiencia de al menos 3 años en el área, preferentemente en trabajos de censos o proyectos agropecuarios</small></li>
          <li><small>Experiencia en manejo de grupos de trabajo y en el uso de herramientas informáticas</small></li>
          <li><small>Que residan en la zona mencionada o en la cabecera distrital (deseable)</small></li>
      </ul>
      `
    },
    {
      nombre: 'Técnico Censista para Grandes Fincas',
      duracion: '5 meses',
      descripcion: `
      <ul>
          <li>
              <small>Profesional universitario egresado en:</small>
              <ul>
                  <li><small><b>Ing. Agronómica</b></small></li>
                  <li><small><b>Ciencias Veterinarias</b></small></li>
                  <li><small><b>Zootecnia</b></small></li>
                  <li><small><b>Economía</b></small></li>
                  <li><small><b>Administración agraria</b></small></li>
                  <li><small><b>Administración de empresas</b></small></li>
                  <li><small><b>Marketing</b></small></li>
                  <li><small><b>Matemática – Estadística o carreras afines</b></small></li>
              </ul>
          </li>
          <li><small>Se valorará cursos en: atención al cliente, marketing o afines</small></li>
          <li><small>Con experiencia en trabajos similares de al menos 3 años en adelante</small></li>
          <li><small>Experiencia en proyectos del sector agropecuario</small></li>
          <li><small>Experiencia en gestión territorial</small></li>
          <li><small>Buen manejo de herramientas informáticas</small></li>
      </ul>
      `
    },
    {
      nombre: 'Técnico Capacitador',
      duracion: '5 meses',
      descripcion: `
      <ul>
          <li>
              <small>Preferentemente graduados en:</small>
              <ul>
                  <li><small><b>Ciencias agrarias</b></small></li>
                  <li><small><b>Administración agraria</b></small></li>
                  <li><small><b>Economía o post grado en Didáctica Universitaria, Comunicación Social, Economía Rural, Educación Agraria u otras</b></small></li>
              </ul>
          </li>
          <li><small>Con experiencia mínima de 3 años en el ejercicio de la profesión docente, preferentemente con experiencia en cursos online</small></li>
          <li><small>Con participación en el desarrollo de censos y encuestas</small></li>
          <li><small>Experiencia en atención al cliente</small></li>
          <li><small>Buen manejo de herramientas informáticas</small></li>
      </ul>
      `
    },
    {
      nombre: 'Censista',
      duracion: '1 mes',
      descripcion: `
      <ul>
          <li><small>Estudiante de los últimos años o egresado de tecnicatura agropecuaria o bachiller</small></li>
          <li><small>Que cuente con 18 años cumplidos</small></li>
          <li><small>Con experiencia en trabajo de campo o proyectos agropecuarios</small></li>
          <li><small>Buen manejo de herramientas informáticas y dispositivos móviles (teléfono/tablet)</small></li>
      </ul>
      `
    }
  ]
}
