import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingCargosComponent } from './landing-cargos.component';

const routes: Routes = [
  {
    path: '',
    component: LandingCargosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingCargosRoutingModule { }
