import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingCargosComponent } from './landing-cargos.component';

describe('LandingCargosComponent', () => {
  let component: LandingCargosComponent;
  let fixture: ComponentFixture<LandingCargosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingCargosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingCargosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
