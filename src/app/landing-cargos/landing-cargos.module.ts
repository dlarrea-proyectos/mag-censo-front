import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingCargosRoutingModule } from './landing-cargos-routing.module';
import { LandingCargosComponent } from './landing-cargos.component';

//Primeng
import {ButtonModule} from 'primeng/button';
import { CardModule } from 'primeng/card';
import { TagModule } from 'primeng/tag';

@NgModule({
  declarations: [
    LandingCargosComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    CardModule,
    TagModule,
    LandingCargosRoutingModule
  ]
})
export class LandingCargosModule { }
