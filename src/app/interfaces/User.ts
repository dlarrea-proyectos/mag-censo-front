export interface Persona {
    id: number;
    documento: number;
    nombres: string;
    apellidos: string;
    estadoCivil: string;
    fechaNacimiento: string;
    sexo: string;
    celular: number;
    CiudadId: number | null;
    direccion: string;
    indigena: string;
    UserId: number;
    createdAt: string;
    updatedAt: string;
}

export interface User {
    id: number;
    email: string;
    nombres: string;
    apellidos: string;
    ultimoInicioSesion: string;
    aud: string;
    exp: number;
    iat: number;
    iss: string;
    sub: string;
    cargo?: string;
    rol: string | any;
    persona?:Persona;
    EstadoId?:number;
    logs?:any;
}