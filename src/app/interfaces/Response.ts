export interface Response {
    status: string;
    message: string;
    data: any;
    code: number | null;
    meta: any;
}