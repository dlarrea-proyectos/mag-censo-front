import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { formValidator, fieldValidator } from 'src/utils/formValidator';
import { Response } from '../interfaces/Response';
import { AuthenticationService } from '../services/authentication.service';
import { strongPass } from '../validators/pass-strong.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  formValidator = formValidator;
  fieldValidator = fieldValidator;
  form : FormGroup;
  loading: boolean = false;
  success: boolean = false;
  cargo: string = '';
  cargos: string[] = ['Jefe Departamental', 'Jefe Distrital', 'Supervisor', 
  'Técnico Censista para Grandes Fincas', 'Técnico Capacitador', 'Censista']; 
  @ViewChild('captcha') captchaElement!:ElementRef;

  constructor(private formBuilder:FormBuilder, private authService: AuthenticationService, 
    private router: Router, private messageService: MessageService) {
      
      this.form = this.formBuilder.group({
        documento: ['', [Validators.required, Validators.pattern('[0-9]*')]],
        nombres: ['', [Validators.required]],
        apellidos: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        clave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
        repetirClave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
        captcha: ['', [Validators.required]],
        cargo: ['', [Validators.required]],
      });
      
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.setCaptcha();
  }

  registrarse = () => {
    if(this.form.valid){
      this.loading = true;
      this.authService.register(this.form.value).subscribe({
        next: (response: Response) => {
          this.success = true;
          this.loading = false;
          this.setCaptcha();
          this.form.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'register', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'register', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loading = false;
          this.setCaptcha();
        }
      });
    }
  }

  clavesIguales = (): boolean =>  {
    let clave = this.form?.controls['clave'].value;
    let repetirClave = this.form?.controls['repetirClave'].value;
    return clave != '' && repetirClave != '' && clave === repetirClave;
  }

  setCaptcha = () => {
    let url = `${environment.apiUrl}/authentication/captcha?update=${new Date().getTime()}`;
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    img.src = url;
  }

  onHideSuccess = () => {
    this.form.reset();
    this.setCaptcha();
    this.router.navigate(['/login']);
  }
}