class Persona {
    documento?: number;
    nombres?: string;
    apellidos?: string;
    celular?:number;
}

class Estado {
    nombre?: number | null;
}

export class User {
    persona?: Persona | null;
    estado?: Estado | null;
    nombres?: string;
    apellidos?: string;
    activo?: string;
    email?: string;
    shortName?: string;
}