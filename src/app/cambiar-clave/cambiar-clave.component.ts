import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { fieldValidator, formValidator } from 'src/utils/formValidator';
import { Response } from '../interfaces/Response';
import { AuthenticationService } from '../services/authentication.service';
import { strongPass } from '../validators/pass-strong.validator';

@Component({
  selector: 'app-cambiar-clave',
  templateUrl: './cambiar-clave.component.html',
  styleUrls: ['./cambiar-clave.component.scss']
})
export class CambiarClaveComponent implements AfterViewInit {

  formValidator = formValidator;
  fieldValidator = fieldValidator;
  form : FormGroup;
  loading: boolean = false;
  @ViewChild('captcha') captchaElement!:ElementRef;

  constructor(private route: ActivatedRoute, private authService: AuthenticationService, 
    private messageService: MessageService, private formBuilder: FormBuilder, private router: Router) {

    this.form = this.formBuilder.group({
      clave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      repetirClave: ['', [Validators.required, Validators.minLength(8), strongPass()]],
      key: ['', [Validators.required]],
      captcha: ['', [Validators.required]],
    });

    this.route.queryParams.subscribe(params => {
      if(params['key']){
        this.form.controls['key'].setValue(params['key']);
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  ngAfterViewInit(): void {
    this.setCaptcha();
  }

  cambiar = () => {
    if(this.form.valid){
      this.loading = true;
      this.authService.cambiarClave(this.form.value).subscribe({
        next: (value: Response) => {
          this.messageService.add({key: 'cambiar', severity:'success', summary:'Cambio de contraseña', detail: 'Su contraseña ha sido cambiada con éxito', life: 10000, closable: true});
          this.form.reset();
          this.setCaptcha();
          this.loading = false;
          this.router.navigate(['/login']);
        },
        error: (err: HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'cambiar', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'cambiar', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loading = false;
          this.setCaptcha();
        }
      })
    }
  }

  onHideSuccess = () => {
    this.router.navigate(['/login']);
  }

  clavesIguales = (): boolean =>  {
    let clave = this.form?.controls['clave'].value;
    let repetirClave = this.form?.controls['repetirClave'].value;
    return clave != '' && repetirClave != '' && clave === repetirClave;
  }

  setCaptcha = () => {
    let url = `${environment.apiUrl}/authentication/captcha?update=${new Date().getTime()}`;
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    img.src = url;
  }
}
