import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CambiarClaveRoutingModule } from './cambiar-clave-routing.module';
import { CambiarClaveComponent } from './cambiar-clave.component';

//Primeng
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CambiarClaveComponent
  ],
  imports: [
    CommonModule,
    InputTextModule,
    CardModule,
    ButtonModule,
    ToastModule,
    FormsModule,
    ReactiveFormsModule,
    CambiarClaveRoutingModule
  ]
})
export class CambiarClaveModule { }
